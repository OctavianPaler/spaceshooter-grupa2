﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mover : MonoBehaviour
{

   [SerializeField]
   private float _speed;
	
	void Start () {
		GetComponent<Rigidbody>().velocity = new Vector3(0f,0f,_speed);
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor.Experimental.UIElements;
using UnityEngine;

public class InfiniteBackground : MonoBehaviour
{
   [SerializeField]
   private float _speed;

   private float _size;
	// Use this for initialization
	void Start ()
	{
	   _size = transform.localScale.y;
	}
	
	// Update is called once per frame
	void Update ()
	{

	   float y = Mathf.Repeat(Time.time * _speed, _size);

		transform.position = new Vector3(0f, 0f,-y);
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{

   private Rigidbody _rigidbody;

   [SerializeField] private float _speed;

	void Start ()
	{
	   _rigidbody = GetComponent<Rigidbody>();
	}
	
	void FixedUpdate ()
	{
	   float horizontalMovement = Input.GetAxis("Horizontal");
	   float verticalMovement = Input.GetAxis("Vertical");

	   _rigidbody.velocity = new Vector3(horizontalMovement, 0f, verticalMovement) * _speed;

	}
}

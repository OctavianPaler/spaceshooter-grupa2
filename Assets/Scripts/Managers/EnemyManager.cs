﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class EnemyManager : MonoBehaviour
{
   [Tooltip("This meesage is shown in Editor!")]
   [SerializeField] private List<GameObject> _enemies;
   [SerializeField] private Vector3 _spawnPoint;
   [SerializeField] private int _waveSize;
   [SerializeField] private float _waveDelay;
   [SerializeField] private float _individualDelay;
   [SerializeField] private float _timeBetweenWaves;
   

   private IEnumerator SpawnWaves()
   {
      yield return new WaitForSeconds(_waveDelay);
      while (true)
      {
         for (int i = 0; i < _waveSize; i++)
         {
            GameObject enemy = _enemies[Random.Range(0, _enemies.Count)];
            Instantiate(enemy, _spawnPoint, Quaternion.identity);
            yield return new WaitForSeconds(_individualDelay);
         }
         yield return new WaitForSeconds(_timeBetweenWaves);
      }
      //If is gameOer then break
   }

	// Use this for initialization
	void Start ()
	{
	   StartCoroutine(SpawnWaves());
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Singleton<T> : MonoBehaviour where T : Singleton<T>
{

   private static T _instance;
   private static bool _searchForInstance = true;

   public static bool IsInitialized => _instance != null;

   //public static bool IsInitialized
   //{
   //   get { return _instance != null; }
   //}

   public static T Instance
   {
      get
      {
         if (!IsInitialized && _searchForInstance)
         {
            _searchForInstance = false;
            T[] objects = FindObjectsOfType<T>();
            if (objects.Length == 1)
            {
               _instance = objects[0];
               DontDestroyOnLoad(_instance.transform.root);
            }
            else if (objects.Length > 1)
            {
               Debug.LogErrorFormat("There are more instances of type {0}", typeof(T));
            }
         }
         return _instance;
      }
   }

   protected virtual void Awake()
   {
      if (IsInitialized && _instance != this)
      {
         Destroy(this);
         Debug.LogErrorFormat("Trying to instantiate a second instance of singleton class {0}. Additional Instance was destroyed", GetType().Name);
      }
      else if (!IsInitialized)
      {
         _instance = this as T;
         _searchForInstance = false;
         DontDestroyOnLoad(gameObject);
      }
   }

   protected virtual void OnDestroy()
   {
      if (this == _instance)
      {
         _instance = null;
         _searchForInstance = true;
      }
   }
}
